package com.example.demo.DAO;

import com.example.demo.DTO.Account;
import com.example.demo.DAO.connect.MyConnectionSQL;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlAccess {
    static MyConnectionSQL myConnectionSQL = new MyConnectionSQL();
    public static Connection connection = myConnectionSQL.getConnection();

    public Account checkAccountExist(String userName) {
        try {
            Statement statement = connection.createStatement();
            String sql = "select * from account WHERE userName = '" + userName + "'";
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Account accountDTO = new Account(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4));
                return accountDTO;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void addAccount(String userName, String userPass, String stage) {
        try {
            Statement statement = connection.createStatement();
            String sql = "insert into account (userName,userPass,stage) values('"
                    + userName + "','" + userPass + "','" + stage + "')";
            statement.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Account checkLogin(String userName, String userPass) {
        try {
            Statement statement = connection.createStatement();
            String sql = "select * from account WHERE userName = '" + userName
                    + "' AND userPass = '" + userPass + "'";
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Account accountDTO = new Account(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4));

                return accountDTO;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getResult(String testName) {
        String result = null;
        try {
            Statement statement = connection.createStatement();
            String sql = "select result from testunit WHERE testName = '" + testName + "'";
            ResultSet resultSet = statement.executeQuery(sql);

            resultSet.next();
            result = resultSet.getString(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

}
