$(document).ready(function () {
    $("#btn_submit").click(function () {
        var name = document.querySelector('#name').value.trim();
        var pass = document.querySelector('#password').value.trim();
        var stage = $('input[name=stage]:checked').val();


        var check = true;
        if (name == "" || pass == "" || !$("input:radio[name='stage']").is(":checked")) {
            alert("Please fill out all field");
            check = false;
        }
        if (check == true) {
            var account = {};
            account["userName"] = name;
            account["userPass"] = pass;
            account["stage"] = stage;

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/signup",
                data: JSON.stringify(account),
                dataType: 'json',
                cache: false,
                timeout: 600000,
                success: function (data) {
                    if (data.id == "0") {
                        alert("Account exist !");
                    }
                    else {
                        alert("SignUp success !");
                        window.location.href = "/";
                    }
                },
            });
        }
    })
});