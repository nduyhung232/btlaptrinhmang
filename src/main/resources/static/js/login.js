var login_page = $(".login-page");
var chat_page = $(".chat-page");
var test_page = $(".test-page");
var teacher_page = $(".teacher-page");
var list_chat = document.querySelector('#messageArea');
var username = null;
var userpass = null;
var currentTest = null;

var stompClient = null;

// result object
function result(userName, score) {
    this.userName = userName;
    this.score = score;
}

// list result
var listResult = [];


$(document).ready(function () {

    // click button
    $("#login").click(function () {
        login();
    });

    $("#refresh").click(function () {
        listResult = [];
        myEmpty();
    })

    //  key press
    $("#message-input").keypress(function (e) {
        if (e.which == 13) {
            sendMessage();
        }
    })

    $("#userpass").keypress(function (e) {
        if (e.which == 13) {
            login();
        }
    })

    function login() {
        var account = {};
        username = document.querySelector('#username').value.trim();
        userpass = document.querySelector('#userpass').value.trim();

        account["userName"] = username;
        account["userPass"] = userpass;

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/checkLogin",
            data: JSON.stringify(account),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                if (data.id == "0") {                    // checkLogin sai
                    $("#message-login").html("Wrong Passwork or Username");
                } else {
                    var socket = new SockJS('/ws');
                    stompClient = Stomp.over(socket);

                    stompClient.connect({}, function () {
                        stompClient.subscribe('/topic/public', onMessageReceived);
                        stompClient.send("/app/chat.login",
                            {},
                            JSON.stringify({userName: username})
                        )
                    });

                    login_page.hide();
                    chat_page.show();
                    if (data.stage == "0") {             // student login
                        test_page.show();
                        teacher_page.hide();
                    } else if (data.stage == "1") {      // teacher login
                        test_page.hide();
                        teacher_page.show();
                    }
                    $(".connecting").hide();
                }
            },
        });
    }

    function onMessageReceived(payload) {
        var message = JSON.parse(payload.body);

        if (message.stage == "2") {                      // alert new account login
            var li = document.createElement("li");
            li.innerHTML = message.userName + " joined!";
            li.classList.add("starred");
            list_chat.append(li);

        }
        else if (message.stage == "3") {                // alert account left
            var li = document.createElement("li");
            li.innerHTML = message.userName + " left!";
            li.classList.add("starred");
            list_chat.append(li);
        }
        else if (message.stage == "4") {                 // message show
            var li = document.createElement("li");
            li.innerHTML = message.message;
            li.classList.add("message");
            if (message.userName == username) {
                li.style.marginLeft = "auto";
                li.style.marginRight = "10px";
                li.style.background = "#269deb";
                li.style.color = "white";
            } else {
                li.style.marginLeft = "10px";
                li.style.background = "#f1f0f0";
                li.style.color = "#000";
            }
            list_chat.append(li);
            list_chat.scrollTop = list_chat.scrollHeight;
        }
        else if (message.stage == "5") {                  // set test current
            currentTest = message.message;
            $("#test-name").html("Test " + message.message);

            // disable Submit button
            $("#button-submit").attr('disabled','disabled');
            $("#button-submit").css({background: '#dadada'})
        }
        else if(message.stage == "6"){
            // enable submit button
            $("#button-submit").removeAttr('disabled');
            $("#button-submit").css({background: '#35a5d6'})

        }
        else if (message.stage == "7") {                   // Nhận kết quả cá nhân
            if (message.userName == username) {
                $("#score").html("");
                $("#score").html(message.result + "/30");
            }

            // check userName Rank exist
            var position;
            var check = false;
            for (var i = 0; i < listResult.length; i++) {
                if (listResult[i].userName == message.userName) {
                    check = true
                    position = i;
                }
            }

            if (check == true) {
                listResult[position].score = message.result;
            } else {
                listResult.push(new result(message.userName, message.result));
            }

            mySort();
            myEmpty();

            for (var i = 0; i < listResult.length; i++) {
                $("#table").append("" +
                    "        <tr>\n" +
                    "            <td>\n" +
                    listResult[i].userName +
                    "            </td>\n" +
                    "            <td>\n" +
                    listResult[i].score +
                    "            </td>\n" +
                    "        </tr>")
            }
        }
    }

// select test audio
    $("#table-grid").on("click", ".test-list", function (e) {
        e.preventDefault();

        // select
        $(".test-list").each(function () {
            $(this).css({background: '#fff'});
        })
        $(this).css({background: '#E1F2FE'});
        var str = $(this).html();
        currentTest = str.substring(4, str.length);

        var linkName = "../music/test" + currentTest + ".mp3";
        var audio = document.getElementById('audio');
        var source = document.getElementById('audioSource');
        source.src = linkName;
        audio.load();
    })

        // play audio
    document.getElementById("audio").onplay = function (ev) {
        // add test name
        var message = {
            message: currentTest,
            stage: "5"
        };
        stompClient.send("/app/test.addTestName", {}, JSON.stringify(message));
    }
        // pause audio
    document.getElementById("audio").onpause = function (ev) {
        var message = {
            stage: "6"
        };
        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(message));
    }

    function myEmpty() {
        $("#table").empty();
        $("#table").append(
            "<tr>\n" +
                "<th>" + "Name" + "</th>\n" +
                "<th>" + "Score" + "</th>\n" +
            "</tr>"
        )}

// submit answer
    $("#button-submit").click(function subAnswer() {

        var answer = $('input[name=11]:checked').val() +
            $('input[name=12]:checked').val() +
            $('input[name=13]:checked').val() +
            $('input[name=14]:checked').val() +
            $('input[name=15]:checked').val() +
            $('input[name=16]:checked').val() +
            $('input[name=17]:checked').val() +
            $('input[name=18]:checked').val() +
            $('input[name=19]:checked').val() +
            $('input[name=20]:checked').val() +
            $('input[name=21]:checked').val() +
            $('input[name=22]:checked').val() +
            $('input[name=23]:checked').val() +
            $('input[name=24]:checked').val() +
            $('input[name=25]:checked').val() +
            $('input[name=26]:checked').val() +
            $('input[name=27]:checked').val() +
            $('input[name=28]:checked').val() +
            $('input[name=29]:checked').val() +
            $('input[name=30]:checked').val() +
            $('input[name=31]:checked').val() +
            $('input[name=32]:checked').val() +
            $('input[name=33]:checked').val() +
            $('input[name=34]:checked').val() +
            $('input[name=35]:checked').val() +
            $('input[name=36]:checked').val() +
            $('input[name=37]:checked').val() +
            $('input[name=38]:checked').val() +
            $('input[name=39]:checked').val() +
            $('input[name=40]:checked').val();

        var sendAnswer = {
            userName: username,
            testName: currentTest,
            answer: answer,
            stage: "7"
        };

        stompClient.send("/app/test.mark", {}, JSON.stringify(sendAnswer));
    })
});

function sendMessage() {
    var messageContent = document.querySelector('#message-input').value.trim();

    if (messageContent && stompClient) {
        var chatMessage = {
            userName: username,
            message: messageContent,
            stage: "4"
        };

        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
        document.querySelector('#message-input').value = '';
    }
}

function mySort() {
    var swap;
    for (var i = 0; i < listResult.length; i++) {
        for (var j = i + 1; j < listResult.length; j++) {
            if (listResult[i].score < listResult[j].score) {
                swap = listResult[i];
                listResult[i] = listResult[j];
                listResult[j] = swap;
            }
        }
    }
}
