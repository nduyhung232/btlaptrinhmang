package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.swing.*;

@SpringBootApplication
public class DemoApplication extends JFrame {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
