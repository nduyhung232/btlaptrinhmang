package com.example.demo.api;

import com.example.demo.DAO.SqlAccess;
import com.example.demo.DTO.Account;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ControllerApi {
    SqlAccess sqlAccess = new SqlAccess();

    @PostMapping(value = "/checkLogin")
    public ResponseEntity checkLogin(@RequestBody Account account) {

        Account accountDTO = sqlAccess.checkLogin(account.getUserName(), account.getUserPass());
        if (accountDTO == null) {
            accountDTO = new Account();
            accountDTO.setId(0);        // dang nhap loi
            return ResponseEntity.ok(accountDTO);
        }
        return ResponseEntity.ok(accountDTO);
    }

    @PostMapping(value = "/signup")
    public ResponseEntity signup(@RequestBody Account account) {

        Account accountDTO = sqlAccess.checkAccountExist(account.getUserName());
        if (accountDTO == null) {
            accountDTO = new Account();
            accountDTO.setId(1);        // dang ky thanh cong
            sqlAccess.addAccount(account.getUserName(), account.getUserPass(), account.getStage());
            System.out.println(account.getUserName() + "..." + account.getUserPass() + "..." + account.getStage() + "..." + "Dang ky thanh cong");
            return ResponseEntity.ok(accountDTO);
        } else {
            accountDTO = new Account();
            accountDTO.setId(0);        // dang ky loi
            System.out.println(account.getUserName() + "..." + account.getUserPass() + "..." + account.getStage() + "..." + "Dang ky that bai");

            return ResponseEntity.ok(accountDTO);
        }
    }
}
