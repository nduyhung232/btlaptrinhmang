package com.example.demo.api;

import com.example.demo.DAO.SqlAccess;
import com.example.demo.DTO.Account;
import com.example.demo.DTO.TestUnit;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerWebSocket {
    SqlAccess sqlAccess = new SqlAccess();

    @MessageMapping("/chat.login")
    @SendTo("/topic/public")
    public Account addUser(@Payload Account account, SimpMessageHeaderAccessor headerAccessor) {

        // Add username in web socket session
        headerAccessor.getSessionAttributes().put("username", account.getUserName());

        account.setStage("2");
        return account;
    }

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public Account sendMessage(@Payload Account account) {
        return account;
    }

    @MessageMapping("/test.addTestName")
    @SendTo("/topic/public")
    public Account setTestname(@Payload Account account){
        return account;
    }

    @MessageMapping("/test.mark")
    @SendTo("/topic/public")
    public TestUnit mark(@RequestBody TestUnit testUnit) {

        if (testUnit.getAnswer() != null && testUnit.getTestName() != null) {
            String answerProperly = modify(testUnit.getAnswer());
            testUnit.setTestName("test" + testUnit.getTestName());
            testUnit.setAnswer(answerProperly);
            testUnit.mark();

        }else {
            testUnit.setResult("0");
        }

        return testUnit;
    }
    private String modify(String str){
        str = str.replace("undefined", "0");
        str = str.replace("NaN", "");
        int count = str.length();
        for (int i = 0; i < 30 - count; i++) {
            str = "0"+str;
        }
        return str;
    }

}
