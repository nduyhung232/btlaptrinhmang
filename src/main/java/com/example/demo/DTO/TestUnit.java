package com.example.demo.DTO;

import com.example.demo.DAO.SqlAccess;

public class TestUnit {
    private String userName;
    private String testName;
    private String answer;
    private String result;
    private String stage;

    public void mark() {
        SqlAccess sqlAccess = new SqlAccess();
        int result = 0;
        if (this.testName != null) {
            this.result = sqlAccess.getResult(testName);
            for (int i = 0; i < 30; i++) {
                if (this.answer.substring(i, i + 1).equals(this.result.substring(i, i + 1))) {
                    result++;
                }
            }
        }
        this.result = String.valueOf(result);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
