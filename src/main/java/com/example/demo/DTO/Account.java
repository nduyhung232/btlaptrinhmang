package com.example.demo.DTO;

public class Account {
    private int id;
    private String userName;
    private String userPass;
    private String message;
    private String stage;

    public Account(int id, String userName, String userPass, String stage) {
        this.id = id;
        this.userName = userName;
        this.userPass = userPass;
        this.stage = stage;
    }

    public Account() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }
}
